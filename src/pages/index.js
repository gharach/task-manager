import React, { useState } from 'react';
import TaskForm from '../components/TaskForm';
import TaskList from '../components/TaskList';
import FilterOptions from '../components/FilterOptions';
import Link from 'next/link';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';

const Home = () => {
  const [filter, setFilter] = useState('all');

  return (
    <div>
      <Typography variant="h4">Task Manager</Typography>
      <TaskForm />
      <Stack direction="row" spacing={2} alignItems="center">
        {/* <FilterOptions filter={filter} setFilter={setFilter} /> */}
        <Link href="/tasks">
          View All Tasks
        </Link>
      </Stack>
      <TaskList filter={filter} />
    </div>
  );
};

export default Home;
