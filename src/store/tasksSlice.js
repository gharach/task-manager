import { createSlice } from '@reduxjs/toolkit';

const tasksSlice = createSlice({
  name: 'tasks',
  initialState: [],
  reducers: {
    addTask: (state, action) => {
      state.push(action.payload);
    },
    toggleTask: (state, action) => {
      const task = state.find((t) => t.id === action.payload);
      if (task) {
        task.completed = !task.completed;
      }
    },
    reorderTasks: (state, action) => {
      const { sourceIndex, destinationIndex } = action.payload;
      const [removed] = state.splice(sourceIndex, 1);
      state.splice(destinationIndex, 0, removed);
    },
  },
});

export const { addTask, toggleTask, reorderTasks } = tasksSlice.actions;
export default tasksSlice.reducer;
