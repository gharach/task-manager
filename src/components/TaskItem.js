import React from 'react';
import { useDispatch } from 'react-redux';
import { toggleTask } from '../store/tasksSlice';
import Checkbox from '@mui/material/Checkbox';
import Typography from '@mui/material/Typography';
import { styled } from '@mui/system';

const StyledTaskItem = styled('div')({
  display: 'flex',
  alignItems: 'center',
  marginBottom: '8px',
});

const TaskItem = ({ task }) => {
  const dispatch = useDispatch();

  const handleTaskToggle = () => {
    dispatch(toggleTask(task.id));
  };

  return (
    <StyledTaskItem>
      <Checkbox
        checked={task.completed}
        onChange={handleTaskToggle}
        color="primary"
      />
      <div>
        <Typography
          component="span"
          variant="body1"
          sx={{ textDecoration: task.completed ? 'line-through' : 'none' }}
        >
          {task.title}
        </Typography>
        <Typography variant="body2">{task.description}</Typography>
      </div>
    </StyledTaskItem>
  );
};

export default TaskItem;
