import React from 'react';
import { useDispatch } from 'react-redux';
import { toggleTask } from '../store/tasksSlice';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';

const FilterOptions = ({ filter, setFilter }) => {
  const dispatch = useDispatch();

  const handleFilterChange = (e) => {
    setFilter(e.target.value);
  };

  const handleTaskToggle = () => {
    dispatch(toggleTask(task.id));
  };

  return (
    <FormControl>
      <Select value={filter} onChange={handleFilterChange}>
        <MenuItem value="all">All</MenuItem>
        <MenuItem value="completed">Completed</MenuItem>
        <MenuItem value="incomplete">Incomplete</MenuItem>
      </Select>
    </FormControl>
  );
};

export default FilterOptions;
